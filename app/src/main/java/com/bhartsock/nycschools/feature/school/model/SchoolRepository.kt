package com.bhartsock.nycschools.feature.school.model

import com.bhartsock.nycschools.api.NetworkResponse
import com.bhartsock.nycschools.feature.school.api.SchoolService
import com.bhartsock.nycschools.feature.school.database.SchoolDao
import com.bhartsock.nycschools.utils.networkBoundResource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SchoolRepository @Inject constructor(
    private val schoolService: SchoolService,
    private val schoolDao: SchoolDao
) {

    /**
     * Fetch schools from the api if updateInBackground is true and saved to the database.
     * Returns a Flow<Resource<List<School>>> from the database (source of truth)
     */
    fun getSchools(updateInBackground: Boolean = true) = networkBoundResource(
        query = { schoolDao.getSchoolsDistinct() },
        fetch = { requestSchools() },
        saveFetchResult = { it, _ ->
            if (it is NetworkResponse.Success) {
                schoolDao.insert(it.body)
            }
        },
        shouldFetchFromApi = { updateInBackground }
    )

    /**
     * Get a school from the database by code.
     * @return Flow<School?>
     */
    fun getSchool(code: String) = schoolDao.getSchool(code)

    /**
     * Request schools from the api.
     * @returns ApiResult<List<School>>
     */
    private suspend fun requestSchools() = schoolService.getSchools()
}