package com.bhartsock.nycschools.feature.sats.di

import com.bhartsock.nycschools.database.AppDatabase
import com.bhartsock.nycschools.feature.sats.api.SatService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object SatModule {

    @Provides
    @Singleton
    fun provideSatService(retrofit: Retrofit): SatService =
        retrofit.create(SatService::class.java)

    @Provides
    @Singleton
    fun provideSatDao(database: AppDatabase) = database.satDao()
}