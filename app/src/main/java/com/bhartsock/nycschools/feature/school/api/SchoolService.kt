package com.bhartsock.nycschools.feature.school.api

import com.bhartsock.nycschools.api.ApiResult
import com.bhartsock.nycschools.feature.school.model.School
import retrofit2.http.GET

interface SchoolService {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchools(): ApiResult<List<School>>
}