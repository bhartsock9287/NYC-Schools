package com.bhartsock.nycschools.feature.sats.api

import com.bhartsock.nycschools.api.ApiResult
import com.bhartsock.nycschools.feature.sats.model.Sat
import retrofit2.http.GET

interface SatService {

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSats(): ApiResult<List<Sat>>
}