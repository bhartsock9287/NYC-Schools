package com.bhartsock.nycschools.feature.school.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bhartsock.nycschools.feature.school.model.School
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged

@Dao
abstract class SchoolDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(schools: List<School>)

    @Query("SELECT * FROM School WHERE code LIKE :code")
    abstract fun getSchool(code: String) : Flow<School?>

    @Query("SELECT * FROM School ORDER BY schoolName ASC")
    abstract fun getSchools(): Flow<List<School>>

    fun getSchoolsDistinct() = getSchools().distinctUntilChanged()
}