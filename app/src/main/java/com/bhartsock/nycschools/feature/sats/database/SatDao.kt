package com.bhartsock.nycschools.feature.sats.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bhartsock.nycschools.feature.sats.model.Sat
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged

@Dao
abstract class SatDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(sats: List<Sat>)

    @Query("SELECT * FROM Sat WHERE code LIKE :code")
    abstract fun getSat(code: String) : Flow<Sat?>

    @Query("SELECT * FROM Sat ORDER BY schoolName ASC")
    abstract fun getSats(): Flow<List<Sat>>

    fun getSatsDistinct() = getSats().distinctUntilChanged()
}