package com.bhartsock.nycschools.feature.school.di

import com.bhartsock.nycschools.database.AppDatabase
import com.bhartsock.nycschools.feature.school.api.SchoolService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object SchoolModule {

    @Provides
    @Singleton
    fun provideSchoolService(retrofit: Retrofit): SchoolService =
        retrofit.create(SchoolService::class.java)

    @Provides
    @Singleton
    fun provideSchoolDao(database: AppDatabase) = database.schoolDao()
}