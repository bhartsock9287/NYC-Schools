package com.bhartsock.nycschools.feature.sats.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Sat(
    @PrimaryKey @SerializedName("dbn") val code: String = "",
    @SerializedName("school_name") val schoolName: String = "",
    @SerializedName("num_of_sat_test_takers") val satTestTakers: String = "",
    @SerializedName("sat_critical_reading_avg_score") val readingAvgScore: String = "",
    @SerializedName("sat_math_avg_score") val mathAvgScore: String = "",
    @SerializedName("sat_writing_avg_score") val writingAvgScore: String = ""
) {
    override fun toString(): String =
        "code=$code reading=$readingAvgScore writing=$writingAvgScore math=$mathAvgScore"
}