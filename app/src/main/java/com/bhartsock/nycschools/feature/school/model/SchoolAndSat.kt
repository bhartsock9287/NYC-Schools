package com.bhartsock.nycschools.feature.school.model

import com.bhartsock.nycschools.feature.sats.model.Sat

data class SchoolAndSat(val school: School, val sat: Sat? = null)
