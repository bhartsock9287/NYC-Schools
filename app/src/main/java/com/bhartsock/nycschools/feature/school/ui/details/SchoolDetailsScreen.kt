package com.bhartsock.nycschools.feature.school.ui.details

import android.content.res.Configuration
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bhartsock.nycschools.R
import com.bhartsock.nycschools.feature.sats.model.Sat
import com.bhartsock.nycschools.feature.school.model.School
import com.bhartsock.nycschools.feature.school.model.SchoolAndSat
import com.bhartsock.nycschools.ui.compose.LinkifyText
import com.bhartsock.nycschools.ui.theme.AppTheme

@Composable
fun SchoolDetails(viewModel: SchoolDetailsViewModel, modifier: Modifier = Modifier) {

    val loading by viewModel.loading.observeAsState()
    loading?.let {
        if (it) LinearProgressIndicator(modifier = Modifier.fillMaxWidth())
    }

    val schoolAndSat by viewModel.schoolAndSat.observeAsState()
    schoolAndSat?.let {
        SchoolDetailContent(it, modifier)
    }
}

@Composable
fun SchoolDetailContent(schoolAndSat: SchoolAndSat, modifier: Modifier = Modifier) {
    Surface {
        Column(
            modifier
                .padding(16.dp)
                .verticalScroll(rememberScrollState())
        ) {
            SchoolTitleText(title = schoolAndSat.school.schoolName)
            SchoolDetailsHeaderText(header = schoolAndSat.school.address)
            Spacer(modifier.height(12.dp))
            LinkifyText(
                text = stringResource(
                    id = R.string.school_details_website,
                    schoolAndSat.school.website
                ),
                linkEntire = false
            )
            LinkifyText(
                text = stringResource(
                    id = R.string.school_details_email,
                    schoolAndSat.school.email
                ),
                linkEntire = false
            )
            LinkifyText(
                text = stringResource(
                    id = R.string.school_details_phone,
                    schoolAndSat.school.phone_number
                ),
                linkEntire = false
            )
            SchoolDetailsText(
                details = stringResource(
                    id = R.string.school_details_neighborhood,
                    schoolAndSat.school.neighborhood
                )
            )
            SchoolDetailsText(
                details = stringResource(
                    id = R.string.school_details_borough,
                    schoolAndSat.school.borough
                )
            )
            SchoolDetailsText(
                details = stringResource(
                    id = R.string.school_details_city,
                    schoolAndSat.school.city
                )
            )
            SchoolDetailsText(
                details = stringResource(
                    id = R.string.school_details_state,
                    schoolAndSat.school.state
                )
            )
            SchoolDetailsText(
                details = stringResource(
                    id = R.string.school_details_zip,
                    schoolAndSat.school.zip
                )
            )
            Spacer(modifier.height(20.dp))
            SchoolDetailsText(details = schoolAndSat.school.overview)
            Spacer(modifier.height(12.dp))
            SchoolDetailsHeaderText(header = stringResource(id = R.string.school_details_activities))
            Spacer(modifier.height(12.dp))
            SchoolDetailsText(details = schoolAndSat.school.extracurricularActivities)
            Spacer(modifier.height(20.dp))
            SchoolDetailsHeaderText(header = stringResource(id = R.string.school_details_sat_scores))
            Spacer(modifier.height(12.dp))
            schoolAndSat.sat?.let {
                SchoolDetailsText(
                    details = stringResource(
                        id = R.string.school_details_sat_reading,
                        schoolAndSat.sat.readingAvgScore
                    )
                )
                SchoolDetailsText(
                    details = stringResource(
                        id = R.string.school_details_sat_writing,
                        schoolAndSat.sat.writingAvgScore
                    )
                )
                SchoolDetailsText(
                    details = stringResource(
                        id = R.string.school_details_sat_math,
                        schoolAndSat.sat.mathAvgScore
                    )
                )
            } ?: SchoolDetailsText(details = stringResource(id = R.string.school_details_sat_none))
        }
    }
}

@Composable
fun SchoolTitleText(title: String) {
    Text(
        text = title,
        fontWeight = FontWeight.Bold,
        fontSize = 20.sp,
        color = colorResource(id = android.R.color.holo_orange_dark),
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentWidth(align = Alignment.CenterHorizontally)
    )
}

@Composable
fun SchoolDetailsHeaderText(header: String) {
    Text(
        text = header,
        fontWeight = FontWeight.Bold,
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentWidth(align = Alignment.CenterHorizontally)
    )
}

@Composable
fun SchoolDetailsText(details: String) {
    Text(
        text = details,
        modifier = Modifier.fillMaxWidth()
    )
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun SchoolDetailContentPreview() {
    val school = School(
        code = "100",
        schoolName = "Messalonskee High School",
        overview = "Messalonskee High School is a public high school located in Oakland, Maine, United States. It serves all high school students in the RSU 18 school unit, which includes Oakland, Sidney, Belgrade, China and Rome. The school was founded in 1969 and currently has slightly more than 700 students enrolled.",
        city = "Oakland",
        address = "131 Messalonskee High Drive",
        zip = "04963",
        state = "ME",
        email = "messalonskee@gmail.com",
        phone_number = "207-123-4567",
        website = "rsu18.org",
        extracurricularActivities = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    )
    val sat = Sat(
        code = "100",
        schoolName = "Messalonskee High School",
        satTestTakers = "370",
        readingAvgScore = "403",
        writingAvgScore = "410",
        mathAvgScore = "397"
    )
    AppTheme {
        SchoolDetailContent(SchoolAndSat(school, sat))
    }
}

@Preview
@Composable
fun SchoolTitlePreview() {
    AppTheme {
        SchoolTitleText("Title Text")
    }
}


@Preview
@Composable
fun SchoolDetailsHeaderPreview() {
    AppTheme {
        SchoolDetailsHeaderText("Header Text")
    }
}

@Preview
@Composable
fun SchoolDetailsPreview() {
    AppTheme {
        SchoolDetailsText("Descriptive Text")
    }
}