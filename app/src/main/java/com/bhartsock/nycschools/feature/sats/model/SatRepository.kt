package com.bhartsock.nycschools.feature.sats.model

import com.bhartsock.nycschools.api.NetworkResponse
import com.bhartsock.nycschools.feature.sats.api.SatService
import com.bhartsock.nycschools.feature.sats.database.SatDao
import com.bhartsock.nycschools.utils.networkBoundResource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SatRepository @Inject constructor(
    private val satService: SatService,
    private val satDao: SatDao
) {
    fun getSats(updateInBackground: Boolean = true) = networkBoundResource(
        query = { satDao.getSatsDistinct() },
        fetch = { requestSats() },
        saveFetchResult = { it, _ ->
            if (it is NetworkResponse.Success) {
                satDao.insert(it.body)
            }
        },
        shouldFetchFromApi = { updateInBackground }
    )

    fun getSat(code: String) = satDao.getSat(code)

    private suspend fun requestSats() = satService.getSats()
}