package com.bhartsock.nycschools.feature.school.ui.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import com.bhartsock.nycschools.ui.compose.DemoApp
import com.bhartsock.nycschools.utils.Constants.ARG_SCHOOL_CODE
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolDetailsActivity : ComponentActivity() {

    private val viewModel: SchoolDetailsViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DemoApp(
                content = {
                    SchoolDetails(
                        viewModel = viewModel,
                        modifier = it
                    )
                }
            )
        }
    }

    companion object {

        @JvmStatic
        fun launch(context: Context, code: String) {
            val intent = Intent(context, SchoolDetailsActivity::class.java)
            intent.putExtra(ARG_SCHOOL_CODE, code)
            context.startActivity(intent)
        }
    }
}