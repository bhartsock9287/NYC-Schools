package com.bhartsock.nycschools.feature.school.ui.list

import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bhartsock.nycschools.feature.sats.model.SatRepository
import com.bhartsock.nycschools.feature.school.model.School
import com.bhartsock.nycschools.feature.school.model.SchoolRepository
import com.bhartsock.nycschools.utils.Resource
import com.bhartsock.nycschools.utils.ScopeProvider
import com.bhartsock.nycschools.utils.getViewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor(
    scopeProvider: ScopeProvider,
    private val schoolRepository: SchoolRepository,
    private val satRepository: SatRepository,
) : ViewModel() {

    // A modified view mode scope used for testing view models
    private val viewModelScope = getViewModelScope(scopeProvider)

    // A loading indicator showing when schools are being fetched
    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    // A full list of schools from the database
    val schools = MutableLiveData<List<School>?>()

    // A list of schools matching the state filter criteria
    private val _filteredSchools = MutableLiveData<List<School>?>()

    // A wrapper for the search text contents
    val state = mutableStateOf(TextFieldValue(""))

    init {
        // Get all schools and sats when this view model is created
        getSchoolsAndSats()
    }

    /**
     * Get Schools Filtered by state search criteria.
     * @return A live data observable of a list of schools.
     *         If no filter is present then the full list of schools is returned.
     */
    fun getSchoolsFiltered(): LiveData<List<School>?> {
        val searchedText = state.value.text
        return if (searchedText.isEmpty()) schools
        else {
            _filteredSchools.value = schools.value?.filter {
                it.schoolName.contains(state.value.text, ignoreCase = true) ||
                        it.city.contains(state.value.text, ignoreCase = true)
            }
            _filteredSchools
        }
    }

    /**
     * Get all schools and sats from network and database.
     * The result is emitted on the list of schools live data.
     * The list of sats is not needed on the list screen but fetching pre-emptively.
     */
    private fun getSchoolsAndSats() {
        viewModelScope.launch(Dispatchers.IO) {
            _loading.postValue(true)
            schoolRepository.getSchools().combine(satRepository.getSats()) {
                schoolsResource, _ ->
                if (schoolsResource is Resource.Success) {
                    schoolsResource.data
                }
                else null

            }.collect {
                if (it != null) _loading.postValue(false)
                schools.postValue(it)
            }
        }
    }
}