package com.bhartsock.nycschools.feature.school.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.bhartsock.nycschools.feature.sats.model.SatRepository
import com.bhartsock.nycschools.feature.school.model.SchoolAndSat
import com.bhartsock.nycschools.feature.school.model.SchoolRepository
import com.bhartsock.nycschools.utils.Constants.ARG_SCHOOL_CODE
import com.bhartsock.nycschools.utils.ScopeProvider
import com.bhartsock.nycschools.utils.getViewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolDetailsViewModel @Inject constructor(
    scopeProvider: ScopeProvider,
    private val schoolRepository: SchoolRepository,
    private val satRepository: SatRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    // A modified view mode scope used for testing view models
    private val viewModelScope = getViewModelScope(scopeProvider)

    // A loading indicator showing when schools are being fetched
    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    // A live data for the selected school
    var schoolAndSat: MutableLiveData<SchoolAndSat> = MutableLiveData<SchoolAndSat>()

    // Incoming argument used to fetch a particular school/sat
    private var schoolCode = savedStateHandle.get<String>(ARG_SCHOOL_CODE)!!

    init {
        // Query for school and sat when this view model is created
        getSchoolAndSat(schoolCode)
    }

    /**
     * Get a the school and sat from the database by code.
     * The result is emitted on the schoolAndSat live data.
     */
    private fun getSchoolAndSat(code: String) {
        viewModelScope.launch {
            _loading.postValue(true)
            schoolRepository.getSchool(code).combine(satRepository.getSat(code)) {
                school, sat ->
                if (school != null) SchoolAndSat(school, sat)
                else null
            }.collect {
                _loading.postValue(false)
                if (it != null) schoolAndSat.postValue(it)
            }
        }
    }
}