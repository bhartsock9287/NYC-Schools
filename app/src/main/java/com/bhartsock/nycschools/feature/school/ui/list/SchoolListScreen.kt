package com.bhartsock.nycschools.feature.school.ui.list

import android.content.res.Configuration
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bhartsock.nycschools.R
import com.bhartsock.nycschools.feature.school.model.School
import com.bhartsock.nycschools.ui.theme.AppTheme
import com.bhartsock.nycschools.utils.Constants.TEST_TAG_LIST
import com.bhartsock.nycschools.utils.Constants.TEST_TAG_SEARCH

@Composable
fun SchoolList(
    viewModel: SchoolListViewModel,
    openSchool: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    Column(
        modifier.padding(vertical = 16.dp)
    ) {
        SchoolListSection(viewModel.state) {
            val loading by viewModel.loading.observeAsState()
            loading?.let {
                if (it) LinearProgressIndicator(modifier = Modifier.fillMaxWidth())
            }

            val schools by viewModel.getSchoolsFiltered().observeAsState()
            schools?.let {
                if (it.isNotEmpty()) {
                    SchoolListItems(
                        schools = it,
                        openSchool
                    )
                } else {
                    Text(
                        text = stringResource(id = R.string.school_list_empty),
                        fontSize = 18.sp,
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentWidth(align = Alignment.CenterHorizontally)
                    )
                }
            }
        }
    }
}

@Composable
fun SchoolListSection(
    state: MutableState<TextFieldValue>,
    content: @Composable () -> Unit
) {
    Column {
        Text(
            text = stringResource(id = R.string.school_list_title),
            fontSize = 18.sp,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colors.primary,
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentWidth(align = Alignment.CenterHorizontally)
        )
        Spacer(Modifier.height(8.dp))
        SchoolListSearchBar(state)
        Spacer(Modifier.height(8.dp))
        content()
    }
}

@Composable
fun SchoolListSearchBar(
    state: MutableState<TextFieldValue>,
    modifier: Modifier = Modifier
) {
    TextField(
        value = state.value,
        leadingIcon = {
            Icon(
                imageVector = Icons.Default.Search,
                contentDescription = null,
                modifier = Modifier
                    .padding(15.dp)
                    .size(24.dp)
            )
        },
        trailingIcon = {
            if (state.value != TextFieldValue("")) {
                IconButton(
                    onClick = {
                        state.value = TextFieldValue("")
                    }
                ) {
                    Icon(
                        imageVector = Icons.Default.Close,
                        contentDescription = null,
                        modifier = Modifier
                            .padding(15.dp)
                            .size(24.dp)
                    )
                }
            }
        },
        singleLine = true,
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = MaterialTheme.colors.surface,
            textColor = MaterialTheme.colors.onSecondary
        ),
        onValueChange = { value -> state.value = value },
        modifier = modifier
            .fillMaxWidth()
            .heightIn(min = 56.dp)
            .testTag(TEST_TAG_SEARCH)
    )
}

@Composable
fun SchoolListItems(schools: List<School>, openSchool: (String) -> Unit) {
    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = Modifier.testTag(TEST_TAG_LIST)
    ) {
        items(schools) {
            SchoolCardItem(code = it.code, name = it.schoolName, city = it.city, openSchool)
        }
    }
}

@Composable
fun SchoolCardItem(code: String, name: String, city: String, openSchool: (String) -> Unit) {
    Card(
        shape = RoundedCornerShape(5.dp),
        backgroundColor = MaterialTheme.colors.surface,
        modifier = Modifier.clickable { openSchool(code) }
    ) {
        Column(modifier = Modifier.padding(8.dp)) {
            Text(
                text = name,
                fontWeight = FontWeight.Bold,
                fontSize = 18.sp,
                color = colorResource(id = android.R.color.holo_orange_dark),
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(Modifier.height(10.dp))
            Text(
                text = city,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.fillMaxWidth()
            )
        }
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun SchoolListPreview() {
    val schools = listOf(
        School(schoolName = "Messalonskee High School", city = "Oakland"),
        School(schoolName = "Messalonskee High School", city = "Oakland"),
        School(schoolName = "Messalonskee High School", city = "Oakland"),
        School(schoolName = "Messalonskee High School", city = "Oakland"),
        School(schoolName = "Messalonskee High School", city = "Oakland"),
        School(schoolName = "Messalonskee High School", city = "Oakland")
    )
    AppTheme {
        val textState = remember { mutableStateOf(TextFieldValue("")) }
        SchoolListSection(textState) {
            SchoolListItems(schools = schools, openSchool = { })
        }
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun SchoolListSearchBarPreview() {
    AppTheme {
        val textState = remember { mutableStateOf(TextFieldValue("")) }
        SchoolListSearchBar(textState, Modifier.padding(8.dp))
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun SchoolCardItemPreview() {
    AppTheme {
        SchoolCardItem(
            code = "123",
            name = "Messalonskee High School",
            city = "Oakland",
            openSchool = { })
    }
}