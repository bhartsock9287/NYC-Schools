package com.bhartsock.nycschools.ui.compose

import android.content.res.Configuration
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.bhartsock.nycschools.R
import com.bhartsock.nycschools.ui.theme.AppTheme

@Composable
fun AppBar() {
    TopAppBar(
        title = { Text(stringResource(id = R.string.app_name)) },
        backgroundColor =
        if (isSystemInDarkTheme()) Color.Transparent
        else MaterialTheme.colors.primary,
        contentColor = Color.White,
        elevation = 0.dp
    )
}


@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun AppBarPreview() {
    AppTheme {
        AppBar()
    }
}