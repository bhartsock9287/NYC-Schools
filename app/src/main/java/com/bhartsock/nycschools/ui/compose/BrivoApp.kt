package com.bhartsock.nycschools.ui.compose

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.bhartsock.nycschools.ui.theme.AppTheme

@Composable
fun DemoApp(content: @Composable (modifier: Modifier) -> Unit) {
    AppTheme {
        Scaffold(
            topBar = { AppBar() }
        ) { padding ->
            content(modifier = Modifier.padding(padding))
        }
    }
}