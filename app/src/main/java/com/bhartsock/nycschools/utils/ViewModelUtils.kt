package com.bhartsock.nycschools.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

fun ViewModel.getViewModelScope(scopeProvider: ScopeProvider) =
    if (scopeProvider.coroutineScope is EmptyViewModelCoroutineScope) this.viewModelScope
    else scopeProvider.coroutineScope