package com.bhartsock.nycschools.utils

object Constants {

    const val TEST_TAG_LIST = "list"
    const val TEST_TAG_SEARCH = "search"

    const val ARG_SCHOOL_CODE = "code"
}