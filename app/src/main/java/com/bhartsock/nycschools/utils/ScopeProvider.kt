package com.bhartsock.nycschools.utils

import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

// In order to test coroutines in some cases, we need to inject our own scope.
// References:
// https://adamhurwitz.medium.com/lukasz-kalnik-similar-to-the-coroutinedispatcher-coroutinescope-needs-to-be-injected-into-the-78ea2fe344b3
// https://stackoverflow.com/questions/62332403/how-to-inject-viewmodelscope-for-android-unit-test-with-kotlin-coroutines/62333592#62333592
//
// In order to use this class, just inject this line in the ViewModel constructor block:
// scopeProvider: ScopeProvider
// And then after the constructor add this line:
// private val viewModelScope = getViewModelScope(scopeProvider)
// then you can use viewModelScope as usual, and it will work in testing environment.

class ScopeProvider(val coroutineScope: CoroutineScope) : CoroutineScope {
    override val coroutineContext = coroutineScope.coroutineContext
}

class EmptyViewModelCoroutineScope : CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = EmptyCoroutineContext
}

