package com.bhartsock.nycschools

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import com.bhartsock.nycschools.feature.school.ui.details.SchoolDetailsActivity
import com.bhartsock.nycschools.feature.school.ui.list.SchoolList
import com.bhartsock.nycschools.feature.school.ui.list.SchoolListViewModel
import com.bhartsock.nycschools.ui.compose.DemoApp
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val viewModel: SchoolListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DemoApp(
                content = {
                    SchoolList(
                        viewModel = viewModel,
                        openSchool = { code -> SchoolDetailsActivity.launch(this, code) },
                        modifier = it
                    )
                }
            )
        }
    }
}