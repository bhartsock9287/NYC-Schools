package com.bhartsock.nycschools.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.bhartsock.nycschools.feature.sats.database.SatDao
import com.bhartsock.nycschools.feature.sats.model.Sat
import com.bhartsock.nycschools.feature.school.database.SchoolDao
import com.bhartsock.nycschools.feature.school.model.School

@Database(
    entities = [
        School::class,
        Sat::class
    ],
    version = 2,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun schoolDao(): SchoolDao

    abstract fun satDao(): SatDao
}