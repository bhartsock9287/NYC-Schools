package com.bhartsock.nycschools.database

import androidx.room.TypeConverter
import com.bhartsock.nycschools.feature.sats.model.Sat
import com.bhartsock.nycschools.feature.school.model.School
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object Converters {

    @TypeConverter
    fun schoolsToString(list: List<School>): String {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun fromSchools(value: String): List<School> {
        val listType = object : TypeToken<ArrayList<School>>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun satsToString(list: List<Sat>): String {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun fromSats(value: String): List<Sat> {
        val listType = object : TypeToken<ArrayList<Sat>>() {}.type
        return Gson().fromJson(value, listType)
    }
}