package com.bhartsock.nycschools.di

import com.bhartsock.nycschools.utils.EmptyViewModelCoroutineScope
import com.bhartsock.nycschools.utils.ScopeProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object CoroutinesModule {

    @Provides
    fun provideCoroutineScopeProvider(): ScopeProvider =
        ScopeProvider(EmptyViewModelCoroutineScope())
}