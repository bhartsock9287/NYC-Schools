package com.bhartsock.nycschools.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import com.bhartsock.nycschools.feature.sats.model.Sat
import com.bhartsock.nycschools.feature.sats.model.SatRepository
import com.bhartsock.nycschools.feature.school.model.School
import com.bhartsock.nycschools.feature.school.model.SchoolAndSat
import com.bhartsock.nycschools.feature.school.model.SchoolRepository
import com.bhartsock.nycschools.feature.school.ui.details.SchoolDetailsViewModel
import com.bhartsock.nycschools.utils.Constants.ARG_SCHOOL_CODE
import com.bhartsock.nycschools.utils.MainCoroutineRule
import com.bhartsock.nycschools.utils.ScopeProvider
import com.bhartsock.nycschools.utils.getOrAwaitValue
import junit.framework.TestCase.assertNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestScope
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.any

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolDetailsViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = MainCoroutineRule()

    private val testScope = TestScope(coroutineRule.testDispatcher)

    private lateinit var schoolRepositoryMock: SchoolRepository
    private lateinit var satRepositoryMock: SatRepository

    private lateinit var SUT: SchoolDetailsViewModel

    @Before
    fun setUp() {
        val scopeProvider = ScopeProvider(testScope)
        schoolRepositoryMock = Mockito.mock(SchoolRepository::class.java)
        satRepositoryMock = Mockito.mock(SatRepository::class.java)
        val savedStateHandle = SavedStateHandle().apply {
            set(ARG_SCHOOL_CODE, "MW25")
        }
        SUT = SchoolDetailsViewModel(
            scopeProvider,
            schoolRepositoryMock,
            satRepositoryMock,
            savedStateHandle
        )
        populateSchoolMock()
    }

    @Test
    fun onInit_schoolAndSatAreFetched() {
        Mockito.verify(schoolRepositoryMock, Mockito.times(1)).getSchool(any())
        Mockito.verify(satRepositoryMock, Mockito.times(1)).getSat(any())
    }

    @Test
    fun schoolAndSatFetched_emitsData() {
        val value = SUT.schoolAndSat.getOrAwaitValue()
        assertThat(value, notNullValue())
        assertThat(value.school.code, `is`("MW25"))
        assertNotNull(value.sat)
        assertThat(value.sat!!.code, `is`("MW25"))
    }

    private fun populateSchoolMock() {
        val school = Mockito.mock(School::class.java)
        Mockito.`when`(school.code).thenReturn("MW25")
        Mockito.`when`(school.schoolName).thenReturn("Mast Way")
        Mockito.`when`(school.city).thenReturn("Lee")

        val sat = Mockito.mock(Sat::class.java)
        Mockito.`when`(sat.code).thenReturn("MW25")
        Mockito.`when`(sat.schoolName).thenReturn("Mast Way")
        Mockito.`when`(sat.readingAvgScore).thenReturn("400")
        Mockito.`when`(sat.writingAvgScore).thenReturn("410")
        Mockito.`when`(sat.mathAvgScore).thenReturn("420")

        SUT.schoolAndSat.value = SchoolAndSat(school, sat)
    }
}