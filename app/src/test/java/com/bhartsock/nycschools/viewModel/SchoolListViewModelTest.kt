package com.bhartsock.nycschools.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.compose.ui.text.input.TextFieldValue
import com.bhartsock.nycschools.feature.sats.model.SatRepository
import com.bhartsock.nycschools.feature.school.model.School
import com.bhartsock.nycschools.feature.school.model.SchoolRepository
import com.bhartsock.nycschools.feature.school.ui.list.SchoolListViewModel
import com.bhartsock.nycschools.utils.MainCoroutineRule
import com.bhartsock.nycschools.utils.ScopeProvider
import com.bhartsock.nycschools.utils.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestScope
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.kotlin.any

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolListViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = MainCoroutineRule()

    private val testScope = TestScope(coroutineRule.testDispatcher)

    private lateinit var schoolRepositoryMock: SchoolRepository
    private lateinit var satRepositoryMock: SatRepository

    private lateinit var SUT: SchoolListViewModel

    @Before
    fun setUp() {
        val scopeProvider = ScopeProvider(testScope)
        schoolRepositoryMock = mock(SchoolRepository::class.java)
        satRepositoryMock = mock(SatRepository::class.java)
        SUT = SchoolListViewModel(scopeProvider, schoolRepositoryMock, satRepositoryMock)
        populateSchoolListMock()
    }

    @Test
    fun onInit_schoolsAndSatsAreFetched() {
        verify(schoolRepositoryMock, times(1)).getSchools(any())
        verify(satRepositoryMock, times(1)).getSats(any())
    }

    @Test
    fun schoolsFetched_emitsSchoolData() {
        SUT.getSchoolsFiltered()
        val value = SUT.schools.getOrAwaitValue()
        assertThat(value, notNullValue())
        assertThat(value!!.size, `is`(5))
    }

    @Test
    fun schoolsFiltered_returnsCorrectSchoolsByName() {
        SUT.state.value = getFilterMock("High")
        val value = SUT.getSchoolsFiltered().getOrAwaitValue()
        assertThat(value, notNullValue())
        assertThat(value!!.size, `is`(2))
    }

    @Test
    fun schoolsFiltered_returnsCorrectSchoolsByCity() {
        SUT.state.value = getFilterMock("Durham")
        val value = SUT.getSchoolsFiltered().getOrAwaitValue()
        assertThat(value, notNullValue())
        assertThat(value!!.size, `is`(1))
    }

    @Test
    fun schoolsFiltered_returnsNoSchools() {
        SUT.state.value = getFilterMock("Empty")
        val value = SUT.getSchoolsFiltered().getOrAwaitValue()
        assertThat(value, notNullValue())
        assertThat(value!!.size, `is`(0))
    }

    private fun populateSchoolListMock() {
        SUT.schools.value = listOf(
            getSchoolMock("Mast Way Elementary", "Lee"),
            getSchoolMock("Mohariment Elementary", "Lee"),
            getSchoolMock("Oyster River High School", "Durham"),
            getSchoolMock("Messalonskee High School", "Oakland"),
            getSchoolMock("Belgrade Central School", "Belgrade")
        )
    }

    private fun getSchoolMock(name: String, city: String) : School {
        val school = mock(School::class.java)
        `when`(school.schoolName).thenReturn(name)
        `when`(school.city).thenReturn(city)
        return school
    }

    private fun getFilterMock(filter: String) : TextFieldValue {
        val textFieldValue = mock(TextFieldValue::class.java)
        `when`(textFieldValue.text).thenReturn(filter)
        return textFieldValue
    }
}