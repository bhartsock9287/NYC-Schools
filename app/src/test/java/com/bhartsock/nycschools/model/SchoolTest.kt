package com.bhartsock.nycschools.model

import com.bhartsock.nycschools.feature.school.model.School
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class SchoolTest {

    private lateinit var school: School

    @Before
    fun setup() {
        school = School(
            code = "100",
            schoolName = "Messalonskee High School",
            overview = "Messalonskee High School is a public high school located in Oakland, Maine, United States. It serves all high school students in the RSU 18 school unit, which includes Oakland, Sidney, Belgrade, China and Rome. The school was founded in 1969 and currently has slightly more than 700 students enrolled.",
            city = "Oakland",
            address = "131 Messalonskee High Drive",
            zip = "04963",
            state = "ME",
            email = "messalonskee@gmail.com",
            phone_number = "207-123-4567",
            website = "rsu18.org"
        )
    }

    @Test
    fun test_default_values() {
        assertEquals("100", school.code)
        assertEquals("Messalonskee High School", school.schoolName)
        assertTrue(school.overview.isEmpty().not())
        assertEquals("Oakland", school.city)
        assertEquals("131 Messalonskee High Drive", school.address)
        assertEquals("04963", school.zip)
        assertEquals("ME", school.state)
        assertEquals("messalonskee@gmail.com", school.email)
        assertEquals("207-123-4567", school.phone_number)
        assertEquals("rsu18.org", school.website)
        assertEquals("", school.borough)
        assertEquals("", school.extracurricularActivities)
        assertEquals("", school.neighborhood)
        assertEquals("", school.requirement1_1)
        assertEquals("", school.requirement2_1)
    }

    @Test
    fun test_toString() {
        assertEquals("Messalonskee High School", school.toString())
    }
}