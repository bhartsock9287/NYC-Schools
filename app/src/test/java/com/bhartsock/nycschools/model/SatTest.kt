package com.bhartsock.nycschools.model

import com.bhartsock.nycschools.feature.sats.model.Sat
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SatTest {

    private lateinit var sat: Sat

    @Before
    fun setup() {
        sat = Sat(
            code = "100",
            schoolName = "Messalonskee High School",
            satTestTakers = "57",
            readingAvgScore = "400",
            writingAvgScore = "407",
            mathAvgScore = "490"
        )
    }

    @Test
    fun test_default_values() {
        Assert.assertEquals("100", sat.code)
        Assert.assertEquals("Messalonskee High School", sat.schoolName)
        Assert.assertEquals("57", sat.satTestTakers)
        Assert.assertEquals("400", sat.readingAvgScore)
        Assert.assertEquals("407", sat.writingAvgScore)
        Assert.assertEquals("490", sat.mathAvgScore)
    }

    @Test
    fun test_toString() {
        Assert.assertEquals("code=100 reading=400 writing=407 math=490", sat.toString())
    }
}