package com.bhartsock.nycschools

import androidx.activity.compose.setContent
import androidx.compose.ui.test.assert
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.isRoot
import com.bhartsock.nycschools.feature.sats.model.SatRepository
import com.bhartsock.nycschools.feature.school.model.SchoolRepository
import com.bhartsock.nycschools.feature.school.ui.details.SchoolDetailsActivity
import com.bhartsock.nycschools.feature.school.ui.list.SchoolList
import com.bhartsock.nycschools.feature.school.ui.list.SchoolListViewModel
import com.bhartsock.nycschools.ui.theme.AppTheme
import com.bhartsock.nycschools.utils.Constants.TEST_TAG_LIST
import com.bhartsock.nycschools.utils.Constants.TEST_TAG_SEARCH
import com.bhartsock.nycschools.utils.ScopeProvider
import com.bhartsock.nycschools.utils.waitFor
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject


@HiltAndroidTest
class MainActivityTest {

    @get:Rule(order = 0)
    val hiltTestRule by lazy { HiltAndroidRule(this) }

    @get:Rule(order = 1)
    val composeTestRule by lazy { createAndroidComposeRule<MainActivity>() }

    @Inject lateinit var schoolRepository: SchoolRepository
    @Inject lateinit var satRepository: SatRepository
    @Inject lateinit var scopeProvider: ScopeProvider

    private lateinit var viewModel: SchoolListViewModel

    @Before
    fun setup() {
        hiltTestRule.inject()
        viewModel = SchoolListViewModel(scopeProvider, schoolRepository, satRepository)
        composeTestRule.activity.setContent {
            AppTheme {
                SchoolList(
                    viewModel = viewModel,
                    openSchool = { code -> SchoolDetailsActivity.launch(composeTestRule.activity, code) }
                )
            }
        }

        // Wait 5 seconds while views populate
        onView(isRoot()).perform(waitFor(5000))
    }

    @Test
    fun test_schoolsLoaded() {
        composeTestRule.onNodeWithTag(TEST_TAG_LIST).assertIsDisplayed()
        assertNotNull(viewModel.schools.value)
        assertEquals(440, viewModel.schools.value!!.size)
    }

    @Test
    fun test_tapSchool_opensDetails() {
       composeTestRule.onNodeWithTag(TEST_TAG_LIST).onChildAt(5).performClick()
    }

    @Test
    fun test_searchSchool() {
        composeTestRule.onNodeWithTag(TEST_TAG_SEARCH).apply {
            assertIsDisplayed()
            performTextInput("clinton")
            assert(hasText("clinton"))
        }
        assertNotNull(viewModel.schools.value)
    }
}