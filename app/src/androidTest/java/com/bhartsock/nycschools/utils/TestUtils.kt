package com.bhartsock.nycschools.utils

import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isRoot
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Description
import org.hamcrest.Matcher

// A view action to wait a period of time.
// Useful for when views are not yet available due to data loading times.
fun waitFor(millis: Long) = object : ViewAction {

    override fun getDescription() = "Waiting for $millis milliseconds."

    override fun getConstraints() = isRoot()

    override fun perform(uiController: UiController, view: View) {
        uiController.loopMainThreadForAtLeast(millis)
    }
}

// A view action to perform a query on a SearchView.
fun typeSearchViewText(text: String) = object : ViewAction {

    override fun getDescription() = "Updating view text"

    override fun getConstraints() = allOf(isDisplayed(), isAssignableFrom(SearchView::class.java))

    override fun perform(uiController: UiController, view: View) {
        (view as SearchView).setQuery(text, false)
    }
}

// A matcher to retrieve a view item at a position in a RecyclerView.
fun atPosition(position: Int, itemMatcher: Matcher<View>) =

    object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {

        override fun describeTo(description: Description) {
            description.appendText("has item at position $position: ")
            itemMatcher.describeTo(description)
        }

        override fun matchesSafely(view: RecyclerView): Boolean {
            val viewHolder = view.findViewHolderForAdapterPosition(position)
                ?: return false
            return itemMatcher.matches(viewHolder.itemView)
        }
    }

// A matcher to return the list size of a RecyclerView's adapter.
fun hasListSize(count: Int): Matcher<View> =

    object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {

        override fun describeTo(description: Description) {
            description.appendText("list size: $count")
        }

        override fun matchesSafely(recyclerView: RecyclerView) =
            count == recyclerView.adapter!!.itemCount
}
