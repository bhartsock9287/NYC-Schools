# NYC Schools Project

![Android](https://img.shields.io/badge/platform-android-green.svg)

This is a sample repository for a NYC Schools project that I created to demonstrate a clean architecture.

### Overview of the NYC Schools App ###

The NYC-Schools App uses a public API to fetch a list of schools in New York City and their SAT scores. 
The list is scrollable with a filtered search view at the top. Typing into the search view will filter the list
of schools by name or city. Tapping on a school displays a basic detail screen.

### App Architecture ###

When redesigning this app, I followed Google's suggestions in [Guide to App Architecture]
(https://developer.android.com/topic/architecture). The architecture is MVVM.

The libraries used are:

- Database: Room
- Api calls: Retrofit
- Dependency Injection: Hilt
- Tasks: Kotlin coroutines
- UI: Compose
- Testing: Espresso, Mockito, JUnit

The app uses single source of truth pattern, which means for each entity example `School`, there is
a repository named `SchoolRepository.kt`, which is responsible for api calls and database access for
this model. Whenever the app requests schools, this repo will serve these schools from the database
and update them in the background from the api. Since the connection to the database is `Reactive`,
once the data is updated in the background from the api, the repo will save them in the database
and the views will be notified automatically of the new data. The ViewModel has a reference of this 
repo and through it can perform several actions. The ViewModel doesn't have to know anything about
the dao or the service for this feature (Separation of concerns).

### App Structure ###

The app uses package-by-feature structure for structuring files and folders. There are two features 
in this project, `school` and `sat`. Each feature should contain the following packages:

- api (Api package)
- database (Database package)
- di (Dependency Injection package)
- model (Models package)
- ui (User Interface package)

* In the `api` package:

  Everything related to creating and receiving api calls for this feature. There is the Service
  interface which contains all of the api calls related to this feature. For example 
  `SchoolService.kt` is a service file based on the `Retrofit` library interface.

  In this package would also contain Request/Response files of api calls related to this package.

* In the `database` package:

  Everything related to database for this feature.

  There is the Dao interface or abstract class, which contains all the logic related to database
  calls. For example `SchoolDao.kt` is a dao file is based on `Room` library.

* In the `di` package:

  Everything related to dependency injection, which is based on the `Hilt` library.

  If the feature requires a `Component`, then it is placed here.

  The module for this feature is placed here, example `SchoolModule`

* In the `model` package:

  Everything related to models for this feature.

  There is the `Repository` class, for example `SchoolRepository`.

  All the models of this feature are also in this class, example `School`.

* In the `ui` package:

  Everything related to ui and screens for this feature.

  In the `school` feature, there are two internal screens such as `Scool List Screen` and
  `School Details Screen`. Each screen should have its own package inside the `ui` package.

### Testing ###

In the project we have the following three source sets by default. They are:

* main: Contains your app code. This code is shared amongst all different versions of the app you
  can build (known as build variants)
* androidTest: Contains tests known as instrumented tests.
* test: Contains tests known as local tests.

The difference between local tests and instrumented tests is in the way they are run.

* Local tests (test source set)

  These tests are run locally on your development machine's JVM and do
  not require an emulator or physical device. Because of this, they run fast, but their fidelity is
  lower, meaning they act less like they would in the real world.

  The `SchoolListViewModelTest` tests the ViewModel while mocking out the `SchoolRepository`. This
  allows the view model to be tested in isolation. This class ensures that the list of schools is
  fetched and that the search filter performs as expected (with results and no results).

  The `SchoolDetailsViewModelTest` tests the ViewModel while mocking out the `SchoolRepository`. This
  allows the view model to be tested in isolation. This class ensures that the selected school code 
  passed in as an argument is fetched from the repository.

  The `SchoolTest` and `SatTest` are a simple unit tests on the domain model to ensure default values are working as
  expected.

* Instrumented tests (androidTest source set)
  
  These tests run on real or emulated Android devices. They reflect what will happen in the real
  world, but are also much slower.

  The `MainActivityTest` tests the school list ui is populated, the search text can be filtered,
  and tapping on a school loads the details screen.
